----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:28:53 09/30/2016 
-- Design Name: 
-- Module Name:    SDRAM-IDE - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity SDRAM_IDE is
    Port ( A : in  STD_LOGIC_VECTOR (31 downto 0);
           D : inout  STD_LOGIC_VECTOR (3 downto 0);
           SIZ : in  STD_LOGIC_VECTOR (1 downto 0);
           nDSACK : out  STD_LOGIC_VECTOR (1 downto 0);
           ARAM : out  STD_LOGIC_VECTOR (12 downto 0);
           DQ : out  STD_LOGIC_VECTOR (3 downto 0);
           RAS : out  STD_LOGIC;
           CAS : out  STD_LOGIC;
           MEM_WE : out  STD_LOGIC;
           CLK_RAM : out  STD_LOGIC;
           CLK_EN : out  STD_LOGIC;
           BA : out  STD_LOGIC_VECTOR (1 downto 0);
           RW : in  STD_LOGIC;
           nRAM_SEL : out  STD_LOGIC;
           LE_30_RAM : out  STD_LOGIC;
           OE_30_RAM : out  STD_LOGIC;
           LE_RAM_30 : out  STD_LOGIC;
           OE_RAM_30 : out  STD_LOGIC;
           CBREQ : in  STD_LOGIC;
           CBACK : out  STD_LOGIC;
           CIIN : out  STD_LOGIC;
           IDE_CS : out  STD_LOGIC_VECTOR (1 downto 0);
           IDE_A : out  STD_LOGIC_VECTOR (2 downto 0);
           IDE_R : out  STD_LOGIC;
           IDE_W : out  STD_LOGIC;
           IDE_WAIT : in  STD_LOGIC;
           IDE_RESET : out  STD_LOGIC;
		   IDE_BUFFER_DIR : out  STD_LOGIC;
		   CLK : in  STD_LOGIC;
           ROM_B : out  STD_LOGIC_VECTOR (1 downto 0);
           ROM_EN : out  STD_LOGIC;
           ROM_OE : out  STD_LOGIC;
           ROM_WE : out  STD_LOGIC;
           STERM : out  STD_LOGIC;
           DOUBLE_CLK : in  STD_LOGIC;
           nAS : in  STD_LOGIC;
           nDS : in  STD_LOGIC;
           RESET : in  STD_LOGIC;
           ECS : in  STD_LOGIC;
		   nBOSS : out  STD_LOGIC; 
		   BGACK00_IN : in  STD_LOGIC; 
		   BGACK00 : out  STD_LOGIC; 
		   CPU_SWITCH_PIN : in  STD_LOGIC;
		   AUTO_BOOT : in  STD_LOGIC;
		   HALF_SPEED : in  STD_LOGIC;
		   AC_OUT : out  STD_LOGIC
		   );
end SDRAM_IDE;

architecture Behavioral of SDRAM_IDE is

Function to_std_logic(X: in Boolean) return Std_Logic is
   variable ret : std_logic;
	begin
		if x then 
			ret := '1';  
		else 
			ret := '0'; 
		end if;
   return ret;
end to_std_logic;
	
function MAX(LEFT, RIGHT: INTEGER) return INTEGER is
begin
	if LEFT > RIGHT then 
		return LEFT;
	else 
		return RIGHT;
	end if;
end;

constant MEMORY_128MB: STD_LOGIC:= '1';
constant DEBUG_FEATURES: STD_LOGIC := '0';

constant AUTO_PRECHARGE : STD_LOGIC := '0';
constant PRECHARGE_ALL : STD_LOGIC := '1';
constant NQ_TIMEOUT : integer := 5; --cl2
constant FW_VERSION : STD_LOGIC_VECTOR(3 downto 0) := x"3";

	--wait this number of cycles for a refresh
	--should be 60ns minus one cycle, because the refresh command counts too 150mhz= 6,66ns *9 =60ns
	--puls one cycle for safety :(

constant RQ_TIMEOUT : integer := 700;
	--8192 refreshes in 64ms ->8192 refreshes in 3200000 50MHz ticks
	-- -> Refresh after 390 tics ->370 is a safe place to be!


	TYPE sdram_state_machine_type IS (
				powerup, 					
				init_precharge,			
				init_precharge_commit,  
				init_opcode,				
				init_opcode_wait,	
				init_opcode_wait2,				
				start_state,				
				refresh_start,				
				refresh_wait,				
				commit_ras,			
				start_cas,			
				commit_cas,			
				data_wait,			
				data_wait2,				
				precharge,			
				precharge_wait			
				);
				


signal	MY_CYCLE: STD_LOGIC:='0';
signal   IDE_SPACE:STD_LOGIC:='0';
signal   MEM_SPACE:STD_LOGIC:='0';
signal	AUTO_CONFIG:STD_LOGIC:='0';
signal	AUTO_CONFIG_DONE:STD_LOGIC_VECTOR(1 downto 0):="00";
signal	AUTO_CONFIG_DONE_CYCLE:STD_LOGIC_VECTOR(1 downto 0):="00";
signal	SHUT_UP:STD_LOGIC_VECTOR(1 downto 0) :="11";
signal	Dout1:STD_LOGIC_VECTOR(3 downto 0):=x"F";
signal	Dout2:STD_LOGIC_VECTOR(3 downto 0):=x"F";
signal	D_MUXED:STD_LOGIC_VECTOR(3 downto 0):=x"F";
signal	Dout_ENABLE:STD_LOGIC :='0';
signal	IDE_DSACK_D:STD_LOGIC_VECTOR(2 downto 0):= (others => '0');
signal	IDE_DELAY:STD_LOGIC_VECTOR(2 downto 0):= (others => '0');
signal	DSACK_16BIT:STD_LOGIC :='1';
signal	IDE_ENABLE:STD_LOGIC :='0';
signal	IDE_READY:STD_LOGIC :='0';
signal	ROM_OE_S:STD_LOGIC :='1';
signal	IDE_R_S:STD_LOGIC :='1';
signal	IDE_W_S:STD_LOGIC :='1';
signal	nAS_D0:STD_LOGIC :='1';
signal TRANSFER_IN_PROGRES:STD_LOGIC:= '0';
signal REFRESH: std_logic:= '0';
signal NQ :  STD_LOGIC_VECTOR (3 downto 0):=(others => '0');
signal RQ :  STD_LOGIC_VECTOR (9 downto 0):=(others => '0');
signal CQ :  sdram_state_machine_type:=powerup;  
constant ARAM_OPTCODE: STD_LOGIC_VECTOR (12 downto 0) := "0001000100010"; --cl2
signal ENACLK_PRE : STD_LOGIC :='1';
signal CLK_D0 : STD_LOGIC :='1';
signal STERM_S : STD_LOGIC :='1';
signal STERM_S2 : STD_LOGIC :='1';
signal CBACK_S : STD_LOGIC :='1';
signal RESET_S : STD_LOGIC :='0';
signal burst_counter : STD_LOGIC_VECTOR(1 downto 0) :="11";
signal RAM_BANK_ACTIVATE  :  STD_LOGIC :='0';
signal LE_RAM_CPU_P : STD_LOGIC :='1';
signal CLK_HALF : STD_LOGIC :='1';
signal CPU_SWITCH : STD_LOGIC :='1';
signal BURST : STD_LOGIC :='1';
signal SCAN_CBACK : STD_LOGIC :='1';

--- additional signals for the maprom feature

signal	ROM_SPACE : STD_LOGIC_VECTOR (1 downto 0) :="00";                                                                    
signal	ROM_OVERLAY_ENABLE : STD_LOGIC_VECTOR(3 downto 0) :="0000"; 
signal	CHIP_ROM_OVERLAY_ENABLE : STD_LOGIC:='0';

--- debug feature: if the computer does two consecutive resets without
--- finishing Autoconf, turn of maprom. 

signal	IDE_FIRST_RESET : STD_LOGIC:='0';
signal	RESET_COUNT : STD_LOGIC_VECTOR (1 downto 0) :="00";


attribute fsm_encoding : string;
attribute fsm_encoding of CQ  : signal is "gray";

begin

	CPU_SWITCH <= CPU_SWITCH_PIN;
	nBOSS <= '0' when CPU_SWITCH ='1' or RESET ='0' else '1';
	AC_OUT <= '0' when AUTO_CONFIG_DONE ="11" else '1';

	BGACK00 <= '0' when CPU_SWITCH ='0' or RESET ='0' else BGACK00_IN;

	--internal signals	
	--output
	MY_CYCLE		<= '0' 	when (IDE_SPACE='1' or AUTO_CONFIG ='1' 
										or MEM_SPACE = '1' 
										) else '1';
	nRAM_SEL 	<= MY_CYCLE; 

	--map DSACK signal
	nDSACK		<= "ZZ" when MY_CYCLE ='1' or nAS='1' ELSE
						"01" when DSACK_16BIT	 ='0' else 						
						"01" when AUTO_CONFIG='1' else 
						"11";
	STERM		<= STERM_S2 when nAS ='0' else '1';


	--enable caching for RAM
	CIIN	<= 'Z' when nAS='1' or (MEM_SPACE ='0' and IDE_SPACE = '0' and AUTO_CONFIG = '0' ) else
				'1' when TRANSFER_IN_PROGRES = '1' else
				'0' ;
	CBACK <= CBACK_S when nAS ='0' else '1';

	--SD-RAM clock-stuff
	CLK_RAM 	<= not DOUBLE_CLK;
	CLK_EN 	<= ENACLK_PRE;

	--map signals
	--ide stuff
	
	
	IDE_CS(0)<= not(A(12));			
	IDE_CS(1)<= not(A(13));
	IDE_A(0)	<= A(9);
	IDE_A(1)	<= A(10);
	IDE_A(2)	<= A(11);
	IDE_BUFFER_DIR	<= '0' when nAS='0' and RW='1' and IDE_SPACE ='1' and (A(15)= '0' or IDE_ENABLE='0')else '1';
	IDE_R		<= IDE_R_S when nAS='0' else '1';
	IDE_W		<= IDE_W_S when nAS='0' else '1';
	IDE_RESET<= RESET_S;
	ROM_EN	<= IDE_ENABLE;
	ROM_WE	<= '1';
	ROM_OE	<= ROM_OE_S when nAS='0' else '1';
	ROM_B		<= "00";
	
	--data out for autoconfig(tm)
	D	<=	"ZZZZ" when Dout_ENABLE='0' else
			D_MUXED;	
			
	data_enable:process(nAS,DOUBLE_CLK,RW)begin
		if(RW='0' or nAS='1')then
			Dout_ENABLE <= '0';
			D_MUXED <= "1111";
		elsif(falling_edge(DOUBLE_CLK))then
			if(AUTO_CONFIG ='1' or (IDE_SPACE = '1' and A(15)='1'))then
				Dout_ENABLE <= '1';
			else
				Dout_ENABLE <= '0';
			end if;	
			if(AUTO_CONFIG ='1')then
				if(AUTO_CONFIG_DONE(0)='0')then
					D_MUXED <= Dout1;
				else
					D_MUXED <= Dout2;
				end if;
			else
				case A(14 downto 12) is
					when "100" =>
						D_MUXED <= IDE_DELAY & '0';
				   when "000" =>
						D_MUXED <= ROM_OVERLAY_ENABLE;
					when others =>
						D_MUXED <= FW_VERSION;
				end case;
				
			end if;
		end if;
	end process data_enable;

	
	--transparent latch for writes
	LE_30_RAM <= '0';

	--IDE address decode section 
	address_decode_ide:process(RESET_S,DOUBLE_CLK)begin
		if(RESET_S='0')then
			IDE_SPACE <= '0';
		elsif(falling_edge(DOUBLE_CLK))then
			if(A(31 downto 16) = x"00E9" AND SHUT_UP(1) ='0') then
				IDE_SPACE <= '1';
			else
				IDE_SPACE <= '0';
			end if;			
		end if;
	end process address_decode_ide;

	--memory decode section

	address_decode_mem:process(RESET,DOUBLE_CLK)begin
	
	If (RESET = '0') then
		MEM_SPACE <= '0'; -- maprom tool fails to properly reset.. test
      ROM_SPACE <="00";
	
        elsif ( rising_edge(DOUBLE_CLK)) then
          if (A(31 downto 27) = "01000" and not (
            -- the next line maps the maprom to the end of Z3 RAM. Only used to
            -- debug
            -- (AUTO_CONFIG_DONE(1) = '0' or ROM_OVERLAY_ENABLE(2) = '0') and                                                                            
              A(26 downto 21) = MEMORY_128MB&"11111") and CPU_SWITCH='1') then
            MEM_SPACE <= '1';	
            ROM_SPACE <= "00";
          elsif (A (31 downto 24) = x"00" and 
					  A (23 downto 21) = "111" and 
                ((A (20 downto 19) = "11" and ((ROM_OVERLAY_ENABLE(0) = '1' and RW='1') or (ROM_OVERLAY_ENABLE(1) = '0' and RW='0')))  or
                 (A(19) = '0' and ((ROM_OVERLAY_ENABLE(2) = '1' and RW='1') or (ROM_OVERLAY_ENABLE(3) = '0' and RW='0')))) 		
                 and CPU_SWITCH='1')  	 
            then
              MEM_SPACE <= '1';
              ROM_SPACE <= "01"; --change A(26 downto 21) to '1' at ARAM  access (MapROM is at the end of Z3 RAM).
            elsif ( (A (31 downto 24) = x"00") and (A (23 downto 19) = x"0"&'0') and CHIP_ROM_OVERLAY_ENABLE = '1'and CPU_SWITCH='1') then
              MEM_SPACE <= '1';
             ROM_SPACE <= "10"; --For OVL we need to set A(26 downto 19) to '1'. Requires changing ARAM and BANK.  				 		
          elsif ((A(23 downto 21) = "001" or A(23 downto 21) = "010") and CPU_SWITCH='0') then
            MEM_SPACE <= not SHUT_UP(0);
            ROM_SPACE <="00";
   --       else
   --         MEM_SPACE <= '0';
   --         ROM_SPACE <="00";
          
       else 
          MEM_SPACE <= '0';
          ROM_SPACE <="00";
        end if;
end if;
			
	end process address_decode_mem;

	--Autoconfig(tm) address decode section 
	address_decode_ac:process(RESET_S,DOUBLE_CLK)begin
		if(RESET_S='0')then
			AUTO_CONFIG <= '0';
		elsif(falling_edge(DOUBLE_CLK))then								
			if(((A(31 downto 16) =x"00E8" and CPU_SWITCH ='1') or
				(A(23 downto 16) =x"E8" and CPU_SWITCH ='0'))	
				AND AUTO_CONFIG_DONE /="11") then
				AUTO_CONFIG <= '1';
			else
				AUTO_CONFIG <= '0';
			end if;
		end if;
	end process address_decode_ac;
--
--	IDE_SPACE <= '0';
--	MEM_SPACE <= '0';
--	AUTO_CONFIG <= '0';
--
	psos_edge_no_reset:process (DOUBLE_CLK) begin
      if rising_edge(DOUBLE_CLK) then		
			RESET_S <=RESET;
			
			--nAS delay
			nAS_D0	<=nAS;	
			
			--clk delay
			CLK_D0 <= CLK;
		end if;
	end process;

	--SDRAM-STATEMACHINE
			
	--all signals, which need to be clocked on the negative edge
	neg_edge_ctrl:process(DOUBLE_CLK)
	begin
		if(falling_edge(DOUBLE_CLK))then
			--latch control for reads
			LE_RAM_30<= LE_RAM_CPU_P;
			--STERM_S2		<= STERM_S;
		end if;
	end process neg_edge_ctrl;		
	STERM_S2		<= STERM_S;
	--LE_RAM_30<= LE_RAM_CPU_P;
	--bussizing decode
	bus_siz_ctrl:process (RAM_BANK_ACTIVATE,DOUBLE_CLK)begin
		if(RAM_BANK_ACTIVATE = '0' )then
			BA 	<= "00";
			DQ<= "1111"; --high during init
		elsif(rising_edge(DOUBLE_CLK)) then
			if(MEM_SPACE ='1' and nAS='0' and nAS_D0 = '1') then
				if ( ROM_SPACE = "10" ) then
                                  BA <=  '1'& A(18);
				else
                                  BA <=  A(19 downto 18);
				end if;
				--DQ-mask decoding
				if(RW='0')then --mask on non long writes
					--now decode the adresslines A[0..1] and SIZ[0..1] to determine the ram bank to write				
					-- bits 0-7
					if(	SIZ="00" or 
							(A(0) ='1' and A(1)='1') or 
							(A(1)='1' and SIZ(1)='1') or
							(A(0) ='1' and SIZ="11" ))then
						DQ(0)	<= '0';
					else
						DQ(0)	<= '1';
					end if;
							
					-- bits 8-15
					if(	(A(0) ='0' and A(1)='1') or
							(A(0) ='1' and A(1)='0' and SIZ(0)='0') or
							(A(1)='0' and SIZ="11") or 
							(A(1)='0' and SIZ="00"))then
						DQ(1)<= '0';
					else
						DQ(1)<= '1';
					end if;				
						
					--bits 16-23
					if(	(A(0) ='1' and A(1)='0') or
							(A(1)='0' and SIZ(0)='0') or 
							(A(1)='0' and SIZ(1)='1'))then
						DQ(2)	<= '0';
					else
						DQ(2)	<= '1';
					end if;									
						
					--bits 24--31
					if(( 	A(0) ='0' and A(1)='0' ))then
						DQ(3)	<= '0';
					else
						DQ(3)	<= '1';
					end if;
				else
					DQ <= "0000"; --all others: full 32 bit
				end if;
			end if;
		end if;
	end process bus_siz_ctrl;
 
 	--output buffer control	
	OE_30_RAM <= '0' when TRANSFER_IN_PROGRES='1' and RW='0' else '1';
	OE_RAM_30 <= '0' when TRANSFER_IN_PROGRES='1' and RW='1' else '1';
	
	--MEM address decode section
	--MEM_SPACE <= '1' when A(31 downto 27) = "01000" else '0'; 
	
	--all ram signals, which need to be clocked on the positive edge
	pos_edge_ctrl:process (DOUBLE_CLK,RESET) begin
		if(RESET = '0') then
			RAM_BANK_ACTIVATE <='0';
			CQ	<= powerup;
			RAS <= '1';
			CAS <= '1';
			MEM_WE <= '1';
			ENACLK_PRE <= '1';		 
 			ARAM <= (others => '1');
			RQ<=	(others => '0');
			STERM_S <= '1';
			LE_RAM_CPU_P<= '1';
			TRANSFER_IN_PROGRES <= '0';
			CBACK_S <= '1';
			burst_counter <= "11";
			REFRESH <= '0';
			NQ  <= (others => '0');
			BURST <= '0';
			SCAN_CBACK <= '0';

		elsif rising_edge(DOUBLE_CLK) then			
			--sterm control
			if(	(CQ=commit_cas and RW = '1') or --first read ready
				(CQ=commit_ras and RW = '0')  --write can be terminated one 030-clock earlier
				--or (CQ=commit_ras and HALF_SPEED = '0') --half CPU-Speed = one cycle earlier for read
				)then
				
				STERM_S <= '0' ;
			elsif(nAS = '1' or RESET_S ='0'
				)then
				STERM_S <= '1';
			end if;	

			--transfer detection, cacheburst length and cacheburst acknowledge
			if(nAS='1' or RESET_S ='0')then
				TRANSFER_IN_PROGRES <= '0';
				CBACK_S <= '1';
				burst_counter <= "11";
				BURST <= '0';
			elsif (	CQ=commit_ras	)then --start transfer and decode cacheburst
				TRANSFER_IN_PROGRES <= '1';
				--cache burst logic
				if( CBREQ = '0' 
				    and SIZ="00"
					and A(3 downto 2) /= "11"
					and RW ='1'
					)then
					BURST <= '1';
					CBACK_S <='0';
					burst_counter <= A(3 downto 2);
				else
					BURST <= '0';
					CBACK_S <= '1';
					burst_counter <= "11";
				end if;				
			elsif(CQ=data_wait and CBACK_S ='0')then -- incerement burst counter and stop at the right time
					burst_counter <= burst_counter+1;
			elsif(burst_counter = "11" or CBREQ ='1')then
					CBACK_S <= '1';
			end if;



			--refresh flag
			if(CQ = refresh_start)then
				REFRESH <= '0';
			elsif(RQ = RQ_TIMEOUT) then 
				REFRESH <= '1';
			end if;

			--refresh counter
			if (RQ = RQ_TIMEOUT ) then
				RQ<=	(others => '0');
			else --count on 100-MHz edges
				RQ <= RQ + 1;
			end if;
			
			--wait counter stuff
			if(CQ = refresh_wait)
			then
				NQ <= NQ + 1;
			else 
				NQ  <= (others => '0');
			end if;				
	
			
			-- default values
			ENACLK_PRE <= '1';		 
			RAS <= '1';
			CAS <= '1';
			MEM_WE <= '1';
			ARAM <= A(17 downto 5);
			LE_RAM_CPU_P<= '1';
			SCAN_CBACK <= '0';
	
			
			-- ram state machine decoder
			case CQ is
			when powerup =>
			 CQ <= init_precharge;					 
			when init_precharge =>
			 ARAM(10) <= '1';			
			 RAS <= '0';
			 MEM_WE <= '0';
			 CQ <= init_precharge_commit;			
			when init_precharge_commit =>
			 ARAM <= ARAM_OPTCODE;			
			 CQ <= init_opcode;  
			when init_opcode =>
			 RAS <= '0';
			 CAS <= '0';
			 MEM_WE <= '0';
			 ARAM <= ARAM_OPTCODE;			
			 CQ <= init_opcode_wait;
			when init_opcode_wait =>
			 CQ <= init_opcode_wait2; --opcode needs some time to get in the mem brain
			when init_opcode_wait2 =>
			 CQ <= refresh_start;   --1st refresh
			when start_state =>
			 if (REFRESH = '1') then
				 CQ <= refresh_start;
			 elsif (	MEM_SPACE ='1' and TRANSFER_IN_PROGRES='0' and nAS='0'
						and CLK='0' and CLK_D0 = '1'
						) then
				
				RAS <= '0'; 
				CQ <= commit_ras;
			 end if;			 
			when refresh_start =>
			 RAS <= '0';
			 CAS <= '0';
			 CQ <= refresh_wait;
			when refresh_wait =>
			 if (NQ >= NQ_TIMEOUT) then			--wait 60ns here
				 if(RAM_BANK_ACTIVATE ='0')then
					CQ <= refresh_wait; --init on start up!
				 else
					CQ <= start_state;
				 end if;
 				 RAM_BANK_ACTIVATE <='1'; --now its save to switch on the bank decode
			 else
				 CQ <= refresh_wait;
			 end if;
		   when commit_ras =>
			 CQ <= start_cas; 
			when start_cas =>
			 ARAM( 2 downto 0) <= A(4 downto 2);
			 if ( ROM_SPACE = "00") then
			   ARAM(9 downto 3) <=  A(26 downto 20);
			 else
				ARAM(9 downto 4) <=  "111111";  --ROM is located in last MB
				if ROM_SPACE = "10" then
				ARAM(3) <= '1';
				else 
				ARAM(3) <= A(20);
				end if;
			 end if;
			 ARAM(10) <= AUTO_PRECHARGE; 
			 CAS <= '0';
			 MEM_WE <= RW;
			 CQ <= commit_cas;
			when commit_cas =>
			 if(burst_counter = "11" and RW = '1' )then
				ARAM(10) <= PRECHARGE_ALL; --Precharge termination of the read (no burst)			
				RAS <= '0';
				MEM_WE <= '0';
			 end if;
			 CQ <= data_wait;	 
			 --LE_RAM_CPU_P<= not RW_030;
			 ENACLK_PRE <= not RW; --delay comes one clock later!
			when data_wait => 
			 SCAN_CBACK <= '1';
			 --latch datat on read
			 LE_RAM_CPU_P<= not RW;
			 --initiate precharge on a write
			 if((RW ='0' and AUTO_PRECHARGE ='0'))then
				ARAM(10) <= PRECHARGE_ALL; --Precharge termination of the write			
				RAS <= '0';
				MEM_WE <= '0';
			 end if;
			 
			 --see if we reached the end
			 if(burst_counter = "11")then
			 	if(RW = '1')then
					CQ <= start_state; -- allready precharged in commit_cas or burst_counter = "10"	 				
				else
					CQ <= precharge_wait; --wait need two recovery clocks
				end if;
			 else
				CQ <= data_wait2;
			 end if;
			when data_wait2 =>
			 SCAN_CBACK <= '1';
			 --LE_RAM_CPU_P<= not RW_030;
			 --see if we have to stop the burst
			 if(burst_counter = "11" and RW = '1' and BURST ='1')then
				ARAM(10) <= PRECHARGE_ALL; --Precharge termination of the read burst		
				RAS <= '0';
				MEM_WE <= '0';
			 end if;
			 ENACLK_PRE <= '0';
			 if (CLK='1' and CLK_D0 = '1') or HALF_SPEED ='1' then
				CQ <= data_wait;
			 end if;
			when precharge =>
			 if(AUTO_PRECHARGE ='0')then
				ARAM(10) <= PRECHARGE_ALL;			
				RAS <= '0';
				MEM_WE <= '0';
			 end if;
			 CQ <= start_state; --start is doing the 2nd clock of precharge delay
			when precharge_wait =>
			 CQ <= start_state;  --start is doing the 2nd clock of precharge delay
			end case;
		end if;
   end process pos_edge_ctrl;	

	ac_ctrl:process (DOUBLE_CLK,RESET_S) begin
		if(RESET_S = '0') then
			AUTO_CONFIG_DONE_CYCLE	<= "00";
			AUTO_CONFIG_DONE	<= "00";
			Dout1 <= "1111";
			Dout2 <= "1111";
			SHUT_UP	<= "11";

			--AUTO_CONFIG_D0 <= '0';
      elsif rising_edge(DOUBLE_CLK) then		

			--Autoconfig(tm) data-encoding

			if(nAS= '1' and nAS_D0= '0' )then
				AUTO_CONFIG_DONE <= AUTO_CONFIG_DONE or AUTO_CONFIG_DONE_CYCLE;
			end if;
		
			Dout1 <=	"1111" ;
			Dout2 <=	"1111" ;
					
			case A(6 downto 1) is
				when "000000"	=> 
					--Dout1 <= "1010" ; --ZIII, Memory,  no ROM
					--Dout2 <= "1101" ; --ZII, no Memory,  ROM
					Dout1(2) <=	not CPU_SWITCH ; --ZIII in 68030 or just ZII in 68k mode
					--Dout1(1) <=	'0' ; --activate this line for a memtest system!
					Dout1(0) <=	'0' ;
					Dout2(1) <=	'0' ;
					Dout2(0) <= AUTO_BOOT;
				when "000001"	=>  
					--Dout2 <=	"0001" ; --one Card, 64kb = 001
					Dout1(2) <=	not  CPU_SWITCH ; --128MB AUTOSIZING in 68030 and 4MB in 68k
					Dout2(3 downto 1) <=	"000" ; --one Card, 64kb = 001
				when "000010"	=>  --ProductID high nibble: 
					Dout1 <=	"0010" ; --d0 Dicke Olga
				when "000011"	=> 
					--Dout2 <=	"1001" ; --ProductID low nibble: 9->0110=6
					Dout2(2 downto 1) <=	"00" ;
				when "000100"	=> --er_flags 
					Dout1(3) <=	'0' ; -- memory device
					Dout1(1) <=	not CPU_SWITCH ; -- size extension in 68030mode
				when "000101"	=> 
					--extension bits
					Dout1 <="111" & not CPU_SWITCH; --autosizing in 68030-mode
				when "001001"	=> 
					--Dout1 <=	"0101" ;
					--Dout2 <=	"0111" ; --Ventor ID 1
					Dout1(1) <=	'0' ;
					Dout1(3) <=	'0' ;
					Dout2(3) <=	'0' ;
				when "001010"	=> 
					--Dout1 <=	"1110" ;
					--Dout2 <=	"1101" ; --Ventor ID 2
					Dout1(0) <=	'0' ;
					Dout2(1) <=	'0' ;
					
				when "001011"	=> 
					--Dout1 <=	"0011" ; --Ventor ID 3 : $0A1C: A1k.org
					--Dout2 <=	"0011" ; --Ventor ID 3 : $082C: BSC
					Dout1(3 downto 2) <=	"00" ;
					Dout2(3 downto 2) <=	"00" ;
				when "001100"	=> 
					--Dout1 <=	"1111" ;
					--Dout2 <=	"0100" ; --Serial byte 0 (msb) high nibble
					Dout2(0) <=	'0' ;
					Dout2(1) <=	'0' ;
					Dout2(3) <=	'0' ;
				when "001101"	=> 
					--Dout1 <=	"1111" ;
					--Dout2 <=	"1110" ; --Serial byte 0 (msb) low  nibble
					Dout2(0) <=	'0' ;
				when "001110"	=> 
					--Dout1 <=	"1111" ;
					--Dout2 <=	"1001" ; --Serial byte 1       high nibble
					Dout2(2 downto 1) <=	"00" ;
				when "001111"	=> 
					--Dout1 <=	"1111" ;
					--Dout2 <=	"0100" ; --Serial byte 1       low  nibble
					Dout2(0) <=	'0' ;
					Dout2(1) <=	'0' ;
					Dout2(3) <=	'0' ;
				when "010010"	=> 
					--Dout1 <=	"1111" ;
					--Dout2 <=	"0100" ; --Serial byte 3 (lsb) high nibble
					Dout2(0) <=	'0' ;
					Dout2(1) <=	'0' ;
					Dout2(3) <=	'0' ;
				when "010011"	=> 
					--Dout1 <=	"1111" ;
					--Dout2 <=	"1010" ; --Serial byte 3 (lsb) low  nibble: B16B00B5
					Dout1 <=	not(FW_VERSION);
					Dout2(0) <=	'0' ;
					Dout2(2) <=	'0' ;
				when "010111"	=> 
					--Dout1 <=	"1111" ;
					--Dout2 <=	"1110" ; --Rom vector low byte low  nibble
					Dout2(0) <=	'0' ; --Rom vector low byte low  nibble
				when others	=> 
					Dout1 <=	"1111" ;
					Dout2 <=	"1111" ;
			end case;

			--write stuff
			if(nDS = '0' and RW='0' and AUTO_CONFIG ='1') then
				case A(6 downto 1) is
					when "100100"	=> 
						if(AUTO_CONFIG_DONE(0) = '0')then 
							SHUT_UP(0) <= '0'; --enable board
							AUTO_CONFIG_DONE_CYCLE(0)	<= '1'; --done here
						end if;
						if(AUTO_CONFIG_DONE(0)  = '1')then 
							SHUT_UP(1) <= '0'; --enable board
							AUTO_CONFIG_DONE_CYCLE(1)	<= '1'; --done here
						end if;
					when "100110"	=> 
						if(AUTO_CONFIG_DONE(0) = '0')then 
							AUTO_CONFIG_DONE_CYCLE(0)	<= '1'; --done here
						end if;
						if(AUTO_CONFIG_DONE(0) = '1')then 
							AUTO_CONFIG_DONE_CYCLE(1)	<= '1'; --done here
						end if;
					when others	=> 
				end case;
			end if;
		end if;
   end process ac_ctrl;	


	
	-- this is the clocked ide-process
	ide_rw_gen: process (RESET_S,CLK,ROM_OVERLAY_ENABLE)
	begin
		if(RESET_S = '0')then
			IDE_ENABLE			<='0';
			IDE_R_S		<= '1';
			IDE_W_S		<= '1';
			ROM_OE_S	<= '1';
			IDE_DSACK_D		<= (others => '0');
			DSACK_16BIT			<= '0';
			IDE_DELAY <= "111";	
			IDE_READY <='0';	
			CLK_HALF	<='1';
			if (IDE_FIRST_RESET = '1' and DEBUG_FEATURES='1') then
				IDE_FIRST_RESET <= '0';
				RESET_COUNT <= RESET_COUNT + 1;
                                if (AUTO_CONFIG_DONE(1) = '0' and RESET_COUNT(1) = '1' ) then -- if autoconfig is not completed before a reset, 
                                  ROM_OVERLAY_ENABLE(0) <= '0';         -- the MapROM contents are most likely invalid, so we disable it. Because Scrats
                                  CHIP_ROM_OVERLAY_ENABLE <= '0';       -- MapROM tool does two resets, we need to count to three here. 
                                else
                                  CHIP_ROM_OVERLAY_ENABLE <= ROM_OVERLAY_ENABLE(0); 
                                end if;
			else 
			CHIP_ROM_OVERLAY_ENABLE <= ROM_OVERLAY_ENABLE(0);
			end if;
		 elsif rising_edge(CLK) then
			IDE_FIRST_RESET <= '1';
			
			if (A(23 downto 4) =x"BFE00" and RW = '0') then --detects writes to $BFE001 to reset OVL
				CHIP_ROM_OVERLAY_ENABLE <= '0';
			end if;
			if (AUTO_CONFIG_DONE(1) = '0') then -- once we reach the end of autoconfig, assume correct boot and reset the RESET_COUNT
				RESET_COUNT <= "00";
			end if;
			if(IDE_SPACE = '1' and nAS = '0')then
				CLK_HALF	<= not CLK_HALF;
				if(RW = '0')then
					IDE_R_S		<= '1';
					ROM_OE_S		<= '1';
					--enable IDE on the first write on this IO-space!
					IDE_ENABLE <= '1';

					if (A(15) = '1' and nDS = '0') then
						if (A(14)='1')then
							IDE_DELAY<=D(3 downto 1); --even if we set a value which has already passed, the counter wraps around and terminates this cycle!
						elsif (A(14)='0') then
						ROM_OVERLAY_ENABLE <=  D(3 downto 0);
						end if;
					end if;					
					--the write goes to the hdd!
					IDE_W_S		<= A(15);
					if(IDE_WAIT = '1' and IDE_READY = '1')then --IDE I/O
						DSACK_16BIT		<=	'0';
					end if;

				else
					IDE_W_S		<= '1';

					if(IDE_ENABLE = '1')then
						--read from IDE instead from ROM
						IDE_R_S		<= '0';
						ROM_OE_S		<= '1';
						if(IDE_WAIT = '1' and IDE_READY = '1')then --IDE I/O
							DSACK_16BIT		<=	'0';
						end if;
					else
						DSACK_16BIT		<= not IDE_DSACK_D(2); --hack to reduce complexity! Fortunaterly the cycle terminates fast enough
						IDE_R_S		<= '1';
						ROM_OE_S		<=	'0';	
					end if;
				end if;
				if(CLK_HALF ='0') then
					IDE_DSACK_D		<=	IDE_DSACK_D +1;
				end if;
				if(IDE_DSACK_D=IDE_DELAY)then
					IDE_READY <='1';
				end if;
			else
				--default values
				CLK_HALF	<='1';
				IDE_R_S		<= '1';
				IDE_W_S		<= '1';
				ROM_OE_S		<= '1';
				IDE_DSACK_D	<= (others => '0');
				DSACK_16BIT	<= '1';	
				IDE_READY <='0';				
			end if;		
		end if;
	end process ide_rw_gen;

end Behavioral;
