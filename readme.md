# 68030-RAM-IDE
 
This is the 64MB SD-RAM-expansion for the 68030 [CPU turbo-card](https://gitlab.com/MHeinrichs/68030tk).

![Ram-Card with DOM](https://gitlab.com/MHeinrichs/68030-RAM-IDE/raw/master/Bilder/The%20card.jpg)

The RAM is autoconfig Zorro III mem and requires Kick 2.0 or greater to work!

The SD-RAM runs at 100Mhz and runns a 3-1-1-1 databurst cycle: The IDE can make up to 9MB/s  and provides power via the "IDE-key pin" for some DOMs. However it's very picky to IDE-cable lengths.

## Things to improve
The power supply is done via the VG 96 connector . This is not sufficient for some CF-Cards. These cards need power from somewhere else!

**DISCLAIMER!!!!**  
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA!**

## License
1. Do whatever you want with this, if you do it for private use and mention this work as origin. 
2. If you want to make money out of this work, ask me first!
3. If you copy parts of this work to your project, mention the source.
4. If you use parts of this work for closed source, burn in hell and live in fear!

## Theory of operation
* This PCB has four layers and consists of the two SD-RAM & 32 bit bus width, a PLL-Clock to double the 50MHz clock, a Xilinx XC95144XL, a bootrom, some bus bufferes for the IDE port and latches to provide the 3.3V levelshift for the SD-RAMs. 
* The CPLD does various things:
    * It initializes the SD-RAM and makes the propper refreshes.
    * It makes the Autoconfig for the 64MB in Zorro III
    * It controlls the IDE

## What is in this repository?
This repository consists of several directories:
* PCB: The board, schematic and bill of material (BOM) are in this directory. 
* Logic: Here the code, project files and pin-assignments for the CPLD is stored
* Bilder: Some pictures of this project
* root directory: the readme

## How to open the PCB-files?
You find the eagle board and schematic files (brd/sch) for the pcb. The software can be [downloaded](https://www.autodesk.com/products/eagle/overview) for free after registration. KiCad can import these files too. But importers are not 100% reliable... 

## How to make a PCB?
You can generate Gerber-files from the brd files according to the specs of your PCB-manufactor. However, you can try to use the Gerber-files provided. Some specs: min trace width: 0.15mm/6mil, min trace dist: 0.15mm/6mil, min drill: 0.3mm

**ATTENTION: THE PCB has 4 layers!**

## How to get the parts?
Most parts can be found on digikey or mouser or a parts supplier of your choice. I recoment Alliance  AS16M16SA-6TIN for SDRAM. Winbond works too, but it must be 6ns! 
A list of parts is found in the file [68030-RAM-IDE-V04.bom](https://gitlab.com/MHeinrichs/68030-RAM-IDE/blob/master/PCB/68030-RAM-IDE-V04.bom)

## How to programm the board?
The CPLD must be programmed via Xilinx IMPACT and an apropriate JTAG-dongle. The JED-File is provided. 

## It doesn't work! How to I get help?
For support visit the [a1k-forum](https://www.a1k.org/forum/showthread.php?t=44245). Yes, you need to register and it's German, but writing in English is accepted. For bug-reports write a ticket here ;). 

